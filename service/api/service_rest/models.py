from django.db import models
from django.utils import timezone


class Technician(models.Model):
    name = models.CharField(max_length=50, null=True)
    employee_id = models.CharField(max_length=20, null=True, unique=True)

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50, unique=True, null=True)
    sold = models.BooleanField(default=False)

class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    datetime = models.DateTimeField(default=timezone.now)
    customer = models.CharField(max_length=70)
    reason = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)
    status = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )
