import { useState } from 'react';


function CustomerForm({ getCustomer }) {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    async function handleSubmit(event) {
      event.preventDefault();

      const newCustomer = {
        first_name: firstName,
        last_name: lastName,
        address: address,
        phone_number: phoneNumber,
      };

      const url = 'http://localhost:8090/api/customers/';
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newCustomer),
      });

      if (response.ok) {
        // Reset form fields
        setFirstName('');
        setLastName('');
        setAddress('');
        setPhoneNumber('');

        // Refresh customer list
        getCustomer();
      } else {
        console.error(response);
      }
    }

    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Customer</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <label className="form-label">First Name</label>
            <input
              type="text"
              className="form-control"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>
          <div className="form-floating mb-3">
            <label className="form-label">Last Name</label>
            <input
              type="text"
              className="form-control"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
          <div className="form-floating mb-3">
            <label className="form-label">Address</label>
            <input
              type="text"
              className="form-control"
              value={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </div>
          <div className="form-floating mb-3">
            <label className="form-label">Phone Number</label>
            <input
              type="text"
              className="form-control"
              value={phoneNumber}
              onChange={(e) => setPhoneNumber(e.target.value)}
            />
          </div>
          <button type="submit" className="btn btn-primary">Add Customer</button>
        </form>
        </div>
      </div>
    </div>
    );
  }


export default CustomerForm
