import React, { useState, useEffect } from 'react';


function AppointmentForm() {
    const [vin, setVin] = useState('');
    const [datetime, setDateTime] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [reason, setReason] = useState('');
    const [submission, setSubmission] = useState(false);

        useEffect(() => {
            async function getTechnicians() {
              const technicianUrl = 'http://localhost:8080/api/technicians/';
              const response = await fetch(technicianUrl);
              if (response.ok) {
                  const data = await response.json();
                  setTechnicians(data.technicians);
              }
            }
            getTechnicians();
        }, []);

        async function handleSubmit(event) {
            event.preventDefault();
            const data = {
                vin,
                datetime,
                customer,
                technician,
                reason,
            };


        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
          if (response.ok) {

            event.target.reset();
            setVin('');
            setDateTime('');
            setCustomer('');
            setTechnician('');
            setReason('');
            setSubmission(true);

          } else {
            console.error(response);
          }

    }


    function handleChangeVin(event) {
        const value = event.target.value;
        setVin(value);
    }
    function handleChangeDateTime(event) {
        const value = event.target.value;
        setDateTime(value);
    }
    function handleChangeCustomer(event) {
        const value = event.target.value;
        setCustomer(value);
    }
    function handleChangeTechnician(event) {
        const value = event.target.value;
        setTechnician(value);
    }
    function handleChangeReason(event) {
        const value = event.target.value;
        setReason(value);
    }

      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create an Appointment</h1>
              <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                  <input onChange={handleChangeVin} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                  <label htmlFor="ends">Vin</label>
                </div>

                <div className="form-floating mb-3">
                  <input onChange={handleChangeDateTime} value={datetime} placeholder="Time" required type="datetime-local" name="datetime" id="time" className="form-control" />
                  <label htmlFor="ends">Time</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeCustomer} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                  <label htmlFor="ends">Customer</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleChangeTechnician} value={technician} required name="technician" id="technician" className="form-select">
                    <option value="">Choose a Technician</option>
                    {technicians.map(technician => {
                      return (
                        <option key={technician.employee_id} value={technician.employee_id}>{technician.name}</option>
                      )
                    })}
                  </select>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeReason} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                  <label htmlFor="ends">Reason</label>
                </div>
                </div>
                <button className="btn btn-primary">Set Appointment</button>
              </form>
              {submission && (
                <div className='alert alert-success' id= "appointment set">
                  Appointment set!
                </div>
              )}
            </div>
          </div>
        </div>
      );
    }

    export default AppointmentForm;
