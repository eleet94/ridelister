import React from 'react';

export function HeaderImg({title, subTitle}) {
    return (
        <section>
            {/* <div style={{ backgroundImage: `url(https://images.unsplash.com/photo-1568605117036-5fe5e7bab0b7?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80)`, */}
            <div style={{ backgroundImage: `url(https://images.unsplash.com/photo-1485291571150-772bcfc10da5?auto=format&fit=crop&q=80&w=2728&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)`,
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
                opacity: .9,
                backgroundSize: 'cover'}}>

                <div className="container" style={{minHeight: '550px'}}>
                    <div className="text-center justify-content-center align-self-center">
                        <h1 className="pt-3 pb-2">{title}</h1>
                        <h4>{subTitle}</h4>
                    </div>
                </div>
            </div>
        </section>
    )
}
