import { useState, useEffect } from "react";

function ListAppointments() {

    const [appointments, setAppointments] = useState([]);

	useEffect(() => {
        async function getAppointments () {
            const url = "http://localhost:8080/api/appointments/"
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
            } else {
                console.error(response);
            }

            };
                getAppointments();
        }, []);

    const deleteAppointment = (id) => async () => {
        try {
            const url = `http://localhost:8080/api/appointments/${id}/`;
            const deleteResponse = await fetch(url,
                {
                    method: "delete",
                }
            );

            if (deleteResponse.ok) {
                const appointmentUrl = "http://localhost:8080/api/appointments/";
                const appointmentResponse = await fetch(appointmentUrl);
                const newAppointment = await appointmentResponse.json();

                setAppointments(newAppointment.appointments);
            }

        }
        catch (error) {

        }

    };

    const setAppointment = (id) => async () => {
        try {
            const url = `http://localhost:8080/api/appointments/${id}/`;
            const setResponse = await fetch(url,
                {
                    method: "put",
                    body: JSON.stringify({status: true}),
                    headers: {
                        "Content-Type": "application/json",
                    }
                }
            );
            if (setResponse.ok) {
                const appointmentUrl = "http://localhost:8080/api/appointments/";
                const appointmentResponse = await fetch(appointmentUrl);
                const newAppointment = await appointmentResponse.json();

                setAppointments(newAppointment.appointments);
            }
        }
        catch (error) {

        }
    };

    return (
        <div>
        <h1>Appointments</h1>
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                <th>VIN</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>VIP</th>
                </tr>
            </thead>
        <tbody>
            {appointments.map((appointment) => {
                if (!appointment.completed)
                    return (
                <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td>{ appointment.customer }</td>
                    <td>{new Date(appointment.datetime).toLocaleDateString()}</td>
                    <td>{new Date(appointment.datetime).toLocaleTimeString([],{hour:"2-digit", minute:"2-digit"})}</td>
                    <td>{ appointment.technician.name }</td>
                    <td>{ appointment.reason }</td>
                    <td>
                        <span className={`status ${appointment.vip ? 'vip' : 'not-vip'}`}>
                            <strong>{appointment.vip ? 'YES' : 'NO'}</strong>
                        </span>
                    </td>
                    <td><button className="btn btn-success" onClick={setAppointment(appointment.id)}>Complete</button></td>
                    <td><button className="btn btn-danger" onClick={deleteAppointment(appointment.id)} >Delete</button></td>
                </tr>
            );
            })}
        </tbody>
        </table>
        </div>
    );
  }

  export default ListAppointments;
