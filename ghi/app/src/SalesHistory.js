import React, { useState, useEffect } from 'react';

function SalesHistory() {
  const [sales, setSales] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [filteredSales, setFilteredSales] = useState([]);

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSelectedSalesperson(value);
  };

  useEffect(() => {
    const fetchSales = async () => {
      const salesUrl = 'http://localhost:8090/api/sales/';
      const response = await fetch(salesUrl);

      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      }
    };

    fetchSales();
  }, []);

  useEffect(() => {
    if (selectedSalesperson) {
      const filtered = sales.filter((sale) => sale.salesperson.employee_id === selectedSalesperson);
      setFilteredSales(filtered);
    } else {
      setFilteredSales([]);
    }
  }, [selectedSalesperson, sales]);

  const uniqueSalespeople = [...new Set(sales.map((sale) => sale.salesperson.employee_id))];

  return (
    <div>
      <div className="mt-3 mb-3">
        <label htmlFor="salesperson">Select a Salesperson:</label>
        <select id="salesperson" value={selectedSalesperson} onChange={handleSalespersonChange}>
          <option value="">Select Salesperson</option>
          {uniqueSalespeople.map((employeeId) => {
            const sale = sales.find((sale) => sale.salesperson.employee_id === employeeId);
            return (
              <option key={employeeId} value={employeeId}>
                {`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}
              </option>
            );
          })}
        </select>
      </div>
      <h2>Sales History</h2>
      {selectedSalesperson && filteredSales.length === 0 ? (
        <p>No sales history available for the selected salesperson.</p>
      ) : (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {filteredSales.map((sale) => (
              <tr key={sale.price}>
                <td>{sale.automobile.vin}</td>
                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                <td>{sale.price}</td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
}

export default SalesHistory;
