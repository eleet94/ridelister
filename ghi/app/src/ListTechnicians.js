function ListTechnicians({ technicians }) {
    return (
        <div>
        <h1>Technicians</h1>
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                <th>Technician Name</th>
                <th>Employee ID</th>
                </tr>
            </thead>
        <tbody>
            {technicians.map((technician) => (
                <tr key={technician.id}>

                    <td>{ technician.name }</td>
                    <td>{ technician.employee_id }</td>
                </tr>

        ))}
        </tbody>
        </table>
        </div>
    );
  }

  export default ListTechnicians;
