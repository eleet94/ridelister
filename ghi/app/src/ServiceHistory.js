import React, {useState, useEffect} from 'react'

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState('');
    const [results, setResults] = useState([]);


    useEffect(() => {
        const getAppointments = async () => {
            const url = "http://localhost:8080/api/appointments/";
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();

                setAppointments(data.appointments);
            }
        };
        getAppointments();
        }, []);

    const searchAppointments = (searchValue) => {
        setSearch(searchValue)
        if (search !== '') {
            const filtered = appointments.filter((appointment) => {
                return Object.values(appointment).join('').toLowerCase().includes(search.toLowerCase())
            })
            setResults(filtered)
        } else {
            setResults(appointments)
        }
    }


    const handleInputChange = (e) => {
        searchAppointments(e.target.value);
        };

    const handleButtonClick = () => {
        searchAppointments(search);
        };


    return (
        <div>
        <h1>Service History</h1>
        <div className="input-group">
            <input
            type="search"
            className="form-control"
            aria-describedby="search-addon"
            placeholder="Search all services by VIN"
            aria-label="Search"
            onChange={handleInputChange}
            value={search} />
            <button type="button"
            className="btn btn-primary"
            onClick={handleButtonClick}
            value={search}>Search</button>
        </div>

        <div>

            <table className="table table-striped table-hover">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>VIP</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {search.length > 0 ? (
                    results.map((appointment) => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer}</td>
                                <td>{new Date(appointment.datetime).toLocaleDateString()}</td>
                                <td>{new Date(appointment.datetime).toLocaleTimeString([],{hour:"2-digit", minute:"2-digit"})}</td>
                                <td>{appointment.technician.name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status ? 'Completed' : 'Deleted'}</td>
                            </tr>
                        );
                    })
                ) : (
                    appointments.map((appointment) => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer}</td>
                                <td>{new Date(appointment.datetime).toLocaleDateString()}</td>
                                <td>{new Date(appointment.datetime).toLocaleTimeString([],{hour:"2-digit", minute:"2-digit"})}</td>
                                <td>{appointment.technician.name}</td>
                                <td>
                                    <span className={`status ${appointment.vip ? 'vip' : 'not-vip'}`}>
                                        <strong>{appointment.vip ? 'YES' : 'NO'}</strong>
                                    </span>
                                </td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status ? 'Completed' : 'Deleted'}</td>
                            </tr>
                        );
                    })
                )}
            </tbody>
            </table>
        </div>
        </div>
    )
};

export default ServiceHistory;
