import React from 'react';

function SalespersonList({ salespeople }) {
  return (
    <table className="table table-dark table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {salespeople.map((salesperson) => (
          <tr key={salesperson.id}>
            <td>{salesperson.employee_id}</td>
            <td>{salesperson.first_name}</td>
            <td>{salesperson.last_name}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default SalespersonList;
