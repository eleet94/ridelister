export function CardGroup() {
    return (
        <section>
            <div className="card-group container mt-4 mb-5">
                <div className="card">
                    <img src="https://images.unsplash.com/photo-1582735363590-e407b612db0a?auto=format&fit=crop&q=80&w=2670&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" className="card-img-top" alt="..."/>
                    <div className="card-body">
                    <h5 className="card-title">Borsche</h5>
                    <p className="card-text">A German automobile manufacturer specializing in high-performance sports cars headquartered in Stuttgart, Baden-Württemberg, Germany.</p>
                    <p className="card-text"><small className="text-muted">See available listings</small></p>
                    </div>
                </div>
                <div className="card">
                    <img src="https://images.unsplash.com/photo-1555215695-3004980ad54e?auto=format&fit=crop&q=80&w=2670&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" className="card-img-top" alt="..."/>
                    <div className="card-body">
                    <h5 className="card-title">DMW</h5>
                    <p className="card-text">A multinational manufacturer of luxury vehicles and motorcycles headquartered in Munich, Bavaria, Germany.</p>
                    <p className="card-text"><small className="text-muted">See available listings</small></p>
                    </div>
                </div>
                <div className="card">
                    <img src="https://images.unsplash.com/photo-1546768292-fb12f6c92568?auto=format&fit=crop&q=80&w=2670&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" className="card-img-top" alt="..."/>
                    <div className="card-body">
                    <h5 className="card-title">Pherrari</h5>
                    <p className="card-text">An Italian luxury sports car manufacturer based in Maranello, Italy.</p>
                    <p className="card-text"><small className="text-muted">See available listings</small></p>
                    </div>
                </div>
            </div>
        </section>
    )
}
