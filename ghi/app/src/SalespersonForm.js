import React, { useState } from 'react';

//  a functional component called SalespersonForm that accepts a prop getSalespeople
function SalespersonForm({ getSalespeople }) {
  //These lines use the useState hook from React to define three state variables
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');

  // This function is an event handler for the form submission
  async function handleSubmit(e) {
    // prevents the default form submission behavior
    e.preventDefault();
    // creates a 'data' object with the form field values and prepares the data to be sent to the server.
    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId,
    };

    const url = 'http://localhost:8090/api/salespeople/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    // makes an asynchronous request to the server using fetch and waits for the response
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      // Call the getSalespeople function to update the list of salespeople
      getSalespeople();
    } else {
      console.error(response);
    }

    e.target.reset();
    // navigate('/salespeople');
    //resets form after submission
  }

  function handleFirstNameChange(e) {
    const value = e.target.value;
    setFirstName(value);
  }

  function handleLastNameChange(e) {
    const value = e.target.value;
    setLastName(value);
  }

  function handleEmployeeIdChange(e) {
    const value = e.target.value;
    setEmployeeId(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Salesperson</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFirstNameChange}
                placeholder="First Name"
                required
                type="text"
                id="firstName"
                name="firstName"
                className="form-control"
              />
              <label htmlFor="firstName">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleLastNameChange}
                placeholder="Last Name"
                required
                type="text"
                id="lastName"
                name="lastName"
                className="form-control"
              />
              <label htmlFor="lastName">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleEmployeeIdChange}
                placeholder="Employee ID"
                required
                type="text"
                id="employeeId"
                name="employeeId"
                className="form-control"
              />
              <label htmlFor="employeeId">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalespersonForm;
