import './index.css';
import { HeaderImg } from './HeaderImg';
import { CardGroup } from './CardGroup';

function MainPage() {
  return (
    <div id="home-page">
      <div className='banner'>
        <HeaderImg className='header' title="RideLister" subTitle="A premiere solution for automobile management"/>
      </div>
      <section>
        <div className="container pt-5">
          <h3 classname="align-center">Manage listings, sales, inventory, service appointments and more...</h3>
        </div>
      </section>
      <div>
        <CardGroup />
      </div>
  </div>

  );
}

export default MainPage;
