function ManufacturersList({ manufacturers }) {
  return (
    <table className="table table-dark table-striped">
      <thead>
        <tr>
          <th>name</th>
        </tr>
      </thead>
      <tbody>
        {manufacturers.map((manufacturers) => {
          return (
            <tr key={manufacturers.id}>
              <td>{manufacturers.name}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ManufacturersList;
