function ListAutomobiles({ automobiles }) {
    return (
        <div>
        <h1>Automobiles</h1>
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                <th>Vin</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
                </tr>
            </thead>
        <tbody>
            {automobiles.map((automobile) => (

                <tr key={automobile.id}>
                    <td>{ automobile.vin }</td>
                    <td>{ automobile.color }</td>
                    <td>{ automobile.year }</td>
                    <td>{ automobile.model.name }</td>
                    <td>{ automobile.model.manufacturer.name }</td>
                    <td>{ String(automobile.sold) }</td>
                </tr>

        ))}
        </tbody>
        </table>
        </div>
    );
  }

  export default ListAutomobiles;
