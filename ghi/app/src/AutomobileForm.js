import React, { useState, useEffect } from 'react';


function AutomobileForm() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model_id, setModel] = useState('');
    const [models, setModels] = useState([]);

    useEffect(() => {
        async function getModels() {
          const modelUrl = 'http://localhost:8100/api/models/';
          const response = await fetch(modelUrl);
          if (response.ok) {
              const data = await response.json();
              setModels(data.models);
          }
        }
        getModels();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            color,
            year,
            vin,
            model_id,
        };

    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(automobileUrl, fetchConfig);
      if (response.ok) {
          setColor('');
          setYear('');
          setVin('');
          setModel('');
      } else {
        console.error(response);
      }
      event.target.reset();
}

function handleChangeColor(event) {
    const value = event.target.value;
    setColor(value);
  }

  function handleChangeYear(event) {
    const value = event.target.value;
    setYear(value);
  }

  function handleChangeVin(event) {
    const value = event.target.value;
    setVin(value);
  }

  function handleChangeModel(event) {
    const value = event.target.value;
    setModel(value);
  }

  return (

    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Automobile</h1>
          <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="starts">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeYear} value={year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
              <label htmlFor="ends">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeVin} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="ends">Vin</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeModel} value={model_id} required name="model" id="model" className="form-select">
                <option value="">Choose Model</option>
                {models.map(model => {
                  return (
                    <option key={model.id} value={model.id}>{model.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
