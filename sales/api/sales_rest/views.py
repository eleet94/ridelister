from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from .models import AutomobileVO, Salesperson, Customer, Sale
# Create your views here.

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
      "vin",
      "sold",
    ]

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]
#hi

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]



class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "salesperson",
        "customer",
        "automobile",
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder,
        "salesperson": SalespersonListEncoder,
        "customer": CustomerListEncoder
    }


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_salesperson(request, id):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(id=id).update(**content)
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customer(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False
        )




@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()

        # Create a list to store the formatted sales data
        formatted_sales = []

        # Iterate through each sale and format the data
        for sale in sales:
            formatted_sale = {
                "id": sale.id,
                "price": sale.price,
                "salesperson": {
                    "first_name": sale.salesperson.first_name,
                    "last_name": sale.salesperson.last_name,
                    "employee_id": sale.salesperson.employee_id,
                },
                "customer": {
                    "first_name": sale.customer.first_name,
                    "last_name": sale.customer.last_name,
                },
                "automobile": {
                    "vin": sale.automobile.vin,
                }
            }
            formatted_sales.append(formatted_sale)

        return JsonResponse(
            {"sales": formatted_sales},
            encoder=SaleListEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            salesperson = content['salesperson']
            salesperson = Salesperson.objects.get(first_name=salesperson['first_name'], last_name=salesperson['last_name'])
            content['salesperson'] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {'message': 'Salesperson does not exist'},
                status=400
            )

        try:
            customer = content['customer']
            customer = Customer.objects.get(first_name=customer['first_name'], last_name=customer['last_name'])
            content['customer'] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {'message': 'Customer does not exist'},
                status=400
            )

        try:
            automobile = content['automobile']
            automobile = AutomobileVO.objects.get(vin=automobile['vin'])
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {'message': 'Automobile does not exist'},
                status=400
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )






@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            formatted_sale = {
                "id": sale.id,
                "price": sale.price,
                "salesperson": {
                    "first_name": sale.salesperson.first_name,
                    "last_name": sale.salesperson.last_name,
                    "employee_id": sale.salesperson.employee_id,
                },
                "customer": {
                    "first_name": sale.customer.first_name,
                    "last_name": sale.customer.last_name,
                },
                "automobile": {
                    "vin": sale.automobile.vin,
                }
            }
            return JsonResponse(
                formatted_sale,
                encoder=SaleListEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {'message': 'Sale does not exist'},
                status=404
            )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Sale.objects.filter(id=id).update(**content)
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False
        )
